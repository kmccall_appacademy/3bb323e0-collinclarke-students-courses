require 'byebug'
class Student

 def initialize(first_name, last_name)
  @first_name = first_name
  @last_name = last_name
  @courses = []
 end

 def first_name
   @first_name
 end

 def last_name
   @last_name
 end

 def courses
   @courses
 end

 def name
   [@first_name, @last_name].join(' ')
 end

 def enroll(new_course)
   raise_error if @courses.any?{|course| course.conflicts_with?(new_course)}
   unless new_course.students.include?(self)
     @courses.push(new_course)
     new_course.students.push(self)
   end
 end

 def course_load
   course_load = Hash.new(0)
   courses.each do |course|
     course_load[course.department] += course.credits
   end
   course_load
 end

end
